# 6.2 - Scaling

---

## Quoi ?

Le scaling d'une application se réalise par un **ReplicaSet** [^1]. Le scaling est horizontal et le nombre de pod est fixé en fonction des valeurs définies dans l'objet. Il est également possible de définir un nombre maximal et minimal de réplicas grâce à l'**Horizontal Pod Autoscaling** [^2], le nombre de réplica sera alors défini en fonction de la charge subie par l'application.

## ReplicaSet

Le **ReplicaSet** est un objet de K8S permettant de définir comment l'application doit être **saclée**.

> Bien qu'il soit possible de créer un **ReplicatSet** dans K8S, la manipulation est déconseillée : il est conseillé d'utiliser un **[déploiement](6.1%20-%20Principes%20de%20bases.md)**. Le déploiement d'une application créée uniquement avec des **ReplicaSet** n'est pas automatisé et c'est à l'utilisateur de réaliser manuellement le déploiement **rolling update**, **canary** ou **green and blue**.

Le manifeste de l'objet est pratiquement identique à celui d'un déploiement :

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: frontend
  labels:
    app: guestbook
    tier: frontend
spec:
  replicas: 3
  selector:
    matchLabels:
      tier: frontend
  template:
    metadata:
      labels:
        tier: frontend
    spec:
      containers:
      - name: php-redis
        image: gcr.io/google_samples/gb-frontend:v3
```

**Point d'attention** cependant, si des pods existent avec le label déclaré dans le **ReplicaSet**, les pods lui seront rattachés.

## Horizontal Pod Autoscaling

Un **Horizontal Pod Autoscaling** ou **HPA** est une ressource permettant de définir un nombre maximal et minimal de replicas en fonction de la charge sur l'application. Cette ressource se rattache à un **ReplicatSet** ou à un **Deployment**.

```yaml
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: frontend-scaler
spec:
  scaleTargetRef:
    kind: ReplicaSet
    name: frontend
  minReplicas: 3
  maxReplicas: 10
```

Dans l'exemple précédent, il est indiqué que le **HPA** sera rattaché au **ReplicaSet** nommé frontend.

L'ajout ou la suppression des replicas est basé sur la charge **CPU** ou la consommation de **RAM**. Il est possible de définir des limites pour l'ajout ou la suppression de pods.

```yaml
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: frontend-scaler
spec:
  scaleTargetRef:
    kind: ReplicaSet
    name: frontend
  minReplicas: 3
  maxReplicas: 10
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 50
```

Pour que cette fonctionnalité fonctionne, il faut que le [serveur de métrique](../11.%20Notions%20avancées/11.2%20-%20Ressources%20et%20métriques.md) soit installé et configuré.

D'autres types de métrique peuvent exister en fonction du serveur de métrique installé [^3] [^4].

Licence : [**`CC-BY-SA-4.0`**](https://spdx.org/licenses/CC-BY-SA-4.0.html)

[^1]: https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/
[^2]: https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/
[^3]: https://github.com/kubernetes-sigs/prometheus-adapter
[^4]: https://github.com/stefanprodan/k8s-prom-hpa
