# RBAC

## Date : 16 juin 2022

---

## Quoi ?

Le **RBAC** [^1] (Role-based access control) permet de gérer les droits utilisateurs sur un cluster Kubernetes. Il permet, par exemple, d'autoriser des utilisateurs ou des [ServiceAccount](10.2%20-%20ServiceAccount.md) à lire les méta-données et les logs d'un pod mais d'y refuser toutes modifications.

![Schema illustrant le fonctionnement du RBAC](../Attachments/RBAC.png)
  
## Objets RBAC

### Role

Le `Role` permet de définir des autorisations sur un **namespace du cluster**.

#### Commande pour la création d'un Role

```bash
kubectl create role pod-lecture --verb=get,list,watch --resource=pods,pods/logs
```

#### Manifest pour la création d'un Role

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: default
  name: pod-lecture
rules:
- apiGroups: [""]
  resources: ["pods", "pods/log"]
  verbs: ["get", "watch", "list"]
```

### ClusterRole

Le `ClusterRole` permet de définir des autorisations sur **l'intégralité du cluster**.

#### Commande pour la création d'un ClusterRole

```bash
kubectl create clusterrole pod-lecture \
--verb=watch,list,get \
--resource=pods,pods/log
```

#### Manifeste pour la création d'un ClusterRole

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: pod-lecture
rules:
- apiGroups: [""]
  resources: ["pods", "pods/log"]
  verbs: ["get", "watch", "list"]
```

### RoleBinding

Le `RoleBinding` est un objet permettant de faire le lien entre un **Role** et un utilisateur.

#### Commande pour la création d'un RoleBinding

```bash
kubectl create rolebinding pod-lecture --user=dev --role=pod-lecture
```

#### Manifest pour la création d'un RoleBinding

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleleBinding
metadata:
  namespace: default
  name: pod-lecture
subjects:
- kind: User
  name: dev
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: pod-lecture
  apiGroup: rbac.authorization.k8s.io
```

### ClusterRoleBinding

Le `ClusterRoleBinding` est un objet permettant de faire le lien entre un **ClusterRole** et un utilisateur.

#### Commande pour la création d'un ClusterRoleBinding

```bash
kubectl create clusterrolebinding pod-lecture --user=dev --clusterrole=pod-lecture
```

#### Manifest pour la création d'un ClusterRoleBinding

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: pod-lecture
subjects:
- kind: User
  name: dev
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: pod-lecture
  apiGroup: rbac.authorization.k8s.io
```

Licence : [**`CC-BY-SA-4.0`**](https://spdx.org/licenses/CC-BY-SA-4.0.html)

[^1]: https://kubernetes.io/docs/reference/access-authn-authz/rbac/>
