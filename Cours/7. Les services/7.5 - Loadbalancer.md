# LoadBalancer

---

## Quoi ?

Kubernetes ne fournit pas de loadBalancer nativement c'est une fonctionnalité disponible lorsque le cluster est hébergé chez un cloud provider qui fournit des load balancers. Le service de type load balancer va donc déclencher sa création lors du déploiement du service.

Il existe deux types de load balancers :

- Les load balancers L4 ou network load balancers:

    Ils traitent les données de la couche 4 (ou layer 4) qui sont présentes au niveau du réseau et du transport (TCP/UDP). Ces load balancers ne se concentrent pas sur les informations applicatives, comme le type de contenu, les cookies, la localisation des headers, etc. Ils vont donc rediriger le trafic uniquement en se basant sur les données de la couche réseau.

- Les load balancers L7 ou application load balancers:

    Ils redirigent le trafic en s’appuyant sur les paramètres de la couche applicative, contrairement aux L4. Ces load balancers traitent ainsi un volume de données plus important et se basent sur davantage d’informations. Ceci inclut par exemple les protocoles HTTP, HTTPS et SSL.

Il est quand même possible de mettre en place un load balancer sur un cluster on-premise, avec [metal-lb](https://metallb.universe.tf/) par exemple.

## Exemple de LoadBalancer

```yaml
apiVersion: v1
  kind: Service
  metadata:
    name: rds-db
  spec:
    type: LoadBalancer
    selector:
      app: rds
    ports:
      - protocol: TCP
        port: 6379
        targetPort: 6379
```

Ici on vient créer un LoadBalancer entre tous les pods qui auront le label app `rds`.