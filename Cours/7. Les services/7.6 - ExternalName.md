# ExternalName

---

## Quoi ?

Un service ExternalName est un cas particulier qui utilise des noms DNS.

##  Exemple de ExternalName

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
  namespace: prod
spec:
  type: ExternalName
  externalName: my.database.example.com

```

Cette définition de service permet de mapper un nom de service sur un nom DNS externe.

Dans l’exemple ci-dessus, le service my-service est créé. Il peut donc être appelé par un objet du cluster en requêtant le nom my-service ou my-service.prod.svc.cluster.local. Agissant comme un CNAME, la requête partira vers l'url my.database.example.com
